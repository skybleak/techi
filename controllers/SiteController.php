<?php

namespace app\controllers;

use app\models\News;
use app\models\NewsAdd;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use app\models\Product;


class SiteController extends Controller
{
    CONST SITE_PATH = '/web/site';

    private $request;

    public function __construct($id, $module, $config = [])
    {
        $this->request = Yii::$app->request;

        parent::__construct($id, $module, $config);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        Yii::$app->view->title = 'Техномаркет';
        $products = Product::find()
            ->orderBy('id DESC')
            ->limit(10)
            ->all();

        return $this->render('index.tpl', ['products' => $products]);
    }

    /**
     * Displays login.
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionProduct()
    {
        $request = Yii::$app->request;
        if ($request->get()) {
            $id = (int)$request->get('id');
            $product = Product::find()
                ->where(['id' => $id])
                ->one();
            if (is_null($product)) {
                throw new ForbiddenHttpException('Product not found');
            }

            return $this->render('product.tpl', ['product' => $product]);
        }

        return $this->redirect('/web');
    }

    /**
     * Displays news.
     *
     * @return string
     */
    public function actionNews()
    {
        Yii::$app->view->title = 'Новости';

        $news = News::find()
            ->orderBy('id DESC')
            ->limit(10)
            ->all();

        return $this->render('news.tpl', ['news_all' => $news]);
    }

    /**
     * Displays news_info.
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionNewsinfo()
    {
        Yii::$app->view->title = 'Новости';

        if ($this->request->get()) {
            $id = (int)$this->request->get('id');
            $news = News::find()
                ->where(['id' => $id])
                ->one();
            if (is_null($news)) {
                throw new ForbiddenHttpException('News not found');
            }

            return $this->render('news-info.tpl', ['news' => $news]);
        }

        return $this->redirect(self::SITE_PATH . '/news');
    }

    public function actionNewsadd()
    {
        Yii::$app->view->title = 'Добавить новсть';
        $news = new News();

        $form_model = new NewsAdd();
        if ($form_model->load(\Yii::$app->request->post())) {
            $title = $form_model->title;
            $description = $form_model->description;

            $news->title = $title;
            $news->text = $description;

            $news->save();

            return $this->redirect(self::SITE_PATH . '/news');
        }

        return $this->render('news-add', compact('form_model'));
    }

    public function actionNewsdelete()
    {
        if ($this->request->get()) {
            $id = (int)$this->request->get('id');
            $news = News::find()
                ->where(['id' => $id])
                ->one();
            if (is_null($news)) {
                throw new ForbiddenHttpException('News not found');
            }
            $news->delete();
        }

        return $this->redirect(self::SITE_PATH . '/news');
    }
}

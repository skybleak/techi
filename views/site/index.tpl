<main>
    <section class="popular container clearfix">
        <p class="popular-text">Популярные товары:</p>
        <a href="#" class="popular-btn">Все популярные товары</a>
    </section>
    <section class="catalog-popular container clearfix">
        {foreach $products as $product}
            <div class="catalog-item">
                <figure class="catalog-item-image">
                    <img src="{Yii::$app->request->baseUrl}/uploads/{$product->img}" width="218" height="169" alt="Перфоратор Bosch BFG 6000">
                </figure>
                <div class="catalog-item-buttons">
                    <a href="{Yii::$app->request->getAbsoluteUrl()}site/product?id={$product->id}" class="catalog-button catalog-item-buy">Подробнее</a>
                </div>
                <p class="catalog-item-vendor">{$product->model}</p>
                <p class="catalog-item-price">{$product->price} Р.</p>
                <p class="catalog-item-discount">{$product->price_sale} Р.</p>
            </div>
        {/foreach}
    </section>
</main>
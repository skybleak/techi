<main>
    <section class="news">
        <div class="container">
            <div class="block" style="width: 80%; margin: 20px auto; border: 2px solid black; padding: 20px;">
                <div class="title">
                    <h3>{$news->title}</h3>
                </div>
                <div class="text">
                    <p>
                        {$news->text}
                    </p>
                </div>
                <div class="footer__block d-flex justify-content-between">
                    <div class="data">
                        {$news->date_creation|date_format}
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
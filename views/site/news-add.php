<?php

use yii\base\Widget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<main>
    <section class="news">
        <div class="container">
            <?php $form = ActiveForm::begin() ?>
            <?= $form->field($form_model, 'title')->label('Заголовок'); ?>
            <?= $form->field($form_model, 'description')->label('Описание')->textarea() ?>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            <?php ActiveForm::end() ?>
        </div>
    </section>
</main>

<main>
    <section class="product">
        <div class="product-info d-flex justify-content-around">
            <div class="image_product" style="width: 300px; height: 300px">
                <img src="{Yii::$app->request->baseUrl}/uploads/{$product->img}" alt="" width="100%">
            </div>
            <div class="desc_product">
                <h1><b>Модель:</b> {$product->model}</h1>
                <h2><b>Цена:</b> {$product->price} Р.</h2>
                {if $product->price_sale}
                    <h2><b>Цена со скидкой:</b> {$product->price_sale} Р.</h2>
                {/if}
            </div>
        </div>
    </section>
</main>
<main>
    <section class="news">
        <div class="container">
            <div class="news__action d-flex justify-content-around">
                <div class="clear__div" style="width: 80%;"></div>
                <a href="newsadd" class="btn btn-success">Добавить новость</a>
            </div>
            {if empty($news_all)}
                <h2 style="text-align: center">Новостей пока нет ((((</h2>
            {/if}
            {foreach $news_all as $news}
                <div class="block" style="width: 80%; margin: 20px auto; border: 2px solid black; padding: 20px;">
                    <div class="title">
                        <h3>{$news->title}</h3>
                    </div>
                    <div class="text">
                        <p>
                            {$news->text}
                        </p>
                    </div>
                    <div class="footer__block d-flex justify-content-between">
                        <div class="data">
                            {$news->date_creation|date_format}
                        </div>
                        <div class="action">
                            <a href="newsinfo?id={$news->id}">Смотреть</a>
                            <a href="newsdelete?id={$news->id}">Удалить</a>
                        </div>
                    </div>
                </div>
            {/foreach}
        </div>
    </section>
</main>
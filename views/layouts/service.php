<section class="service">
    <div class="container">
        <div class="service-intro">
            <h2 class="service-title">Сервисы</h2>
            <p class="service-title-info">Хороший интернет-магазин отличается от плохого не только ценами!</p>
            <p class="service-title-info">Мы стараемся изо всех сил чтобы сделать ваши покупки приятными.</p>
        </div>
        <div class="service-columns clearfix">
            <div class="left-column service-list-column">
                <ul>
                    <li>
                        <a href="#" class="service-list-active">Доставка</a>
                    </li>
                    <li>
                        <a href="#">Гарантия</a>
                    </li>
                    <li>
                        <a href="#">Кредит</a>
                    </li>
                </ul>
            </div>
            <div class="right-column service-details-column">
                <section class="service-details-block service-details-delivery service-details-active">
                    <h2 class="service-details-title">Доставка</h2>
                    <p class="service-details-info service-delivery-info">Мы с одовольствием доставим ваш товар
                        прямо к вашему подьезду совершенно бесплатно!</p>
                    <p class="service-details-info service-delivery-info">Ведь мы неплохо заработаем поднимая его на
                        ваш этаж.</p>
                </section>
                <section class="service-details-block service-details-warrianty">
                    <h2 class="service-details-title">Гарантия</h2>
                    <p class="service-details-info service-warrianty-info">Если купленный у нас товар поломается или
                        заискрит, а также в случае пожара, спровоцированного его возгаранием, вы всегда можете быть
                        уверены в нашей гарантии. Мы обменяем сгоревший товар на новый.</p>
                    <p class="service-details-info service-warrianty-info">Дом уж восстановите как-нибудь сами.</p>
                </section>
                <section class="service-details-block service-details-credit">
                    <h2 class="service-details-title">Кредит</h2>
                    <p class="service-details-info service-credit-info">Залезть в долговую яму стало проще!</p>
                    <p class="service-details-info service-credit-info">Кредитные консультанты придут вам на
                        помощь.</p>
                    <a href="#" class="service-btn">Отправить заявку</a>
                </section>
            </div>
        </div>
    </div>
</section>
<section class="company container clearfix">
    <section class="left-column company-promo-column">
        <h2 class="company-promo-title">«Компания Техномарт»</h2>
        <p class="company-promo-info">Настоящий мужской шоппинг начинается здесь! Огромный выбор товаров для ремонта
            и строительства не оставит равнодушными любителей сэкомномть на гастарбайтерах.</p>
        <p class="company-promo-info">Мы можем доставить товар в самые отдаленные точки России! Техномарт работает
            со многими транспортными компаниями:</p>
        <ul class="company-promo-list">
            <li>Деловые Линии</li>
            <li>Автотрейдинг</li>
            <li>ЖелДорЭкспедиция</li>
        </ul>
        <a hre class="right-column company-contacts-column">
            <h2f="#" class="company-about-btn">Подробнее о компании</a>
    </section>
</section>
<footer>
    <div class="footer-bottom-panel">
        <div class="container clearfix">
            <div style="text-align: center; color: #FFF">
                <p>© <?php echo date("Y"); ?> Компания «Техномарт» Все права защищены</p>
            </div>
        </div>
    </div>
</footer>
<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $img
 * @property string $model
 * @property int $price
 * @property int $price_sale
 * @property string|null $date
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['img', 'model', 'price', 'price_sale'], 'required'],
            [['img', 'model'], 'string'],
            [['price', 'price_sale'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'img' => 'Img',
            'model' => 'Model',
            'price' => 'Price',
            'price_sale' => 'Price Sale',
            'date' => 'Date',
        ];
    }
}
